from PPlay.window import *
from PPlay.gameimage import *
from PPlay.sprite import *
from jogo import *

def  define_botao(rota_default, rota_invertido, x, y, mouse, function = None):
    botao = Sprite(rota_default)
    if mouse.is_over_area((x,y),(x+botao.width, y+botao.height)):
        if function:
            if mouse.is_button_pressed(1):
                function()
        botao = Sprite(rota_invertido)
    botao.set_position(x,y)
    botao.draw()

def vai_pro_menu_dificuldade():
    global gamestate
    gamestate = 1

def vai_pro_jogo():
    global gamestate
    global enemies
    reseta_inimigos(enemies)
    gamestate = 2

def vai_pro_menu_quando_aperta_ESC(teclado):
    if teclado.key_pressed("ESC"):
        vai_pro_menu()

def vai_pro_menu():
    global gamestate
    gamestate = 0

def vai_pra_tela_game_over():
    global gamestate
    global enemies
    enemies = []
    gamestate = 3
    

def tela_game_over(window):
    global gamestate
    window.set_background_color((24,24,24))
    window.draw_text("Game Over", window.width/4, window.height/3, size=72, color=(255,0,0), font_name="Arial", bold=False, italic=False)
    window.update()
    time.sleep(2)
    gamestate = 0

def fecha_o_jogo():
    global window
    window.close()

def muda_dificuldade_pra_facil():
    global game_level
    game_level = 1

def muda_dificuldade_pra_medio():
    global game_level
    game_level = 2

def muda_dificuldade_pra_dificil():
    global game_level
    game_level = 3

def vai_pra_tela_de_hanking():
    global gamestate
    gamestate = 4
    update_best_scores()

def update_best_scores():
    global melhores_cinco_scores
    arquivo = open('ranking.txt', 'r')
    scores = arquivo.readlines()
    scores_tratados = {}
    for score in scores:
        score_tratado = score.strip('\n').split(" ")
        if score_tratado:
            scores_tratados[score_tratado[1]] = score_tratado[0]
    scores_ordenados = sorted(scores_tratados.items(), key = lambda scores_tratados: int(scores_tratados[0]) )
    scores_ordenados = scores_ordenados[::-1]
    melhores_cinco_scores = scores_ordenados[:5]
    arquivo.close()

def printa_ranking(window):
    global melhores_cinco_scores
    position_x_axis = 100
    position_y_axis = 50
    for score in melhores_cinco_scores:
        window.draw_text(f"{score[0]} - {score[1]}".upper(), position_x_axis, position_y_axis, size=30, color=(250,250,250), font_name="Arial", bold=False, italic=False)
        position_y_axis += 40

def menu(window, fundo, mouse):
    fundo.draw()
    define_botao("menu/Botão Play.png","menu/Botão Play invertido.png", 244, 71, mouse, vai_pro_jogo)
    define_botao("menu/Botão Dificuldade.png", "menu/Botão Dificuldade invertido.png", 244, 131, mouse, vai_pro_menu_dificuldade)
    define_botao("menu/Botão de ranking.png","menu/Botão de ranking invertido.png", 244, 191, mouse, vai_pra_tela_de_hanking)
    define_botao("menu/Botão quit.png","menu/Botão quit invertido.png", 244, 251, mouse, fecha_o_jogo)



def menu_dificuldade(window, fundo, mouse):
    fundo.draw()
    define_botao("menu_dificuldade/Botão Fácil.png","menu_dificuldade/Botão Fácil invertido.png", 244, 71, mouse, muda_dificuldade_pra_facil)
    define_botao("menu_dificuldade/Botão Médio.png","menu_dificuldade/Botão Médio invertido.png", 244, 131, mouse, muda_dificuldade_pra_medio)
    define_botao("menu_dificuldade/Botão Difícil.png","menu_dificuldade/Botão Difícil invertido.png" ,244, 191, mouse, muda_dificuldade_pra_dificil)
    define_botao("menu_dificuldade/Botão Play invertido.png","menu_dificuldade/Botão Play.png", 519, 262, mouse, vai_pro_jogo)

def tela_ranking(window, fundo, mouse):
    fundo.draw()
    printa_ranking(window)
    define_botao("menu/Botão menu.png","menu/Botão menu invertido.png", 469, 304, mouse, vai_pro_menu)



window = Window(738,371)
window.set_title("Space Invaders")
fundo = GameImage("menu/cenario menu.png")
mouse = window.get_mouse()
teclado = window.get_keyboard()

ship = Sprite("nave.png")
ship.set_position(window.width/2 - ship.width/2, window.height - ship.height )

shots = []
enemies = []

melhores_cinco_scores = []


gamestate = 0
game_level = 2

while True:
    if gamestate == 0:
        menu(window, fundo, mouse)
    elif gamestate == 1:
        menu_dificuldade(window, fundo, mouse)
    elif gamestate == 2:
        jogo(window, ship, shots, enemies, vai_pro_menu_quando_aperta_ESC, vai_pra_tela_game_over)
    elif gamestate == 3:
        tela_game_over(window)
    elif gamestate == 4:
        tela_ranking(window, fundo, mouse)

    window.update()
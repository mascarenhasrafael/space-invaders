from PPlay.sprite import *
from PPlay.collision import *
from random import choice

tempo_desde_ultimo_tiro = 0
direcao_horizontal_inimigos = 1
velocidade_inimigos = 41

soma_fps_de_um_segundo = 1
contador_fps_somados_em_um_segundo = 1
cronometro = 0
fps_jogo = 0

placar = 0
pontos = 3
cronometro_pontuacao = 0

linhas_matriz = 3
colunas_matriz = 2
tamanho_colunas_maximo = 8
tamanho_linhas_maximo = 3

cronometro_tiros_inimigo = 0
distancia_entre_tiros_inimigo = 2
tiros_inimigo = []
velocidade_tiros_inimigo = 20

vidas = 3

ship_hit = False
tempo_do_golpe = 0
desenha_nave = True
cronometro_piscada = 0

def jogo(window, ship, shots, enemies, go_to_menu = False, game_over_handle = False):
    global tiros_inimigo
    global placar
    global vidas
    global linhas_matriz, colunas_matriz

    teclado = window.get_keyboard()
    if go_to_menu:
        go_to_menu(teclado)
    if game_over_handle:
        if is_game_over(enemies,ship, window):
            game_over_handle()
            vidas = 3
            linhas_matriz = 3
            colunas_matriz = 2
            placar = 0

    window.set_background_color((24,24,24))
    fps(window)
    ship_behavior(ship, teclado, window, shots)
    enemies_behavior(enemies, shots, window)
    draw(ship, shots, enemies, tiros_inimigo, window)
    imprime_na_tela(window, "Placar: ", placar, 10, 55)
    imprime_na_tela(window, "vida: ", vidas, 10, 95)


def fps(window):
    global soma_fps_de_um_segundo
    global contador_fps_somados_em_um_segundo
    global cronometro
    global fps_jogo

    delta_time = window.delta_time()

    if delta_time:
        soma_fps_de_um_segundo += 1/delta_time 
        contador_fps_somados_em_um_segundo += 1
        cronometro += delta_time

    imprime_na_tela(window,"FPS: ",fps_jogo, 10, 10)
    if cronometro >= 1:
        fps_jogo = int(soma_fps_de_um_segundo/contador_fps_somados_em_um_segundo)
        soma_fps_de_um_segundo = 0
        contador_fps_somados_em_um_segundo = 0
        cronometro = 0

def imprime_na_tela(window,string, coisa, x = 0, y = 0):
      window.draw_text(f"{string}{coisa}", x, y, size=30, color=(119,121,24), font_name="Arial", bold=True, italic=False)

def is_game_over(enemies, ship, window):
    global vidas
    if vidas <= 0:
        imprime_na_tela(window,"Digite seu nome no terminal", "", 10, window.height/2 )
        window.update()
        salva_placar()
        return True
    for i in range(0,len(enemies)):
        for j in enemies[i]:
            if j and ship:
                if j.y >= (ship.y - ship.height):
                    if Collision.collided(j, ship):
                        return True
    return False

def salva_placar():
    global placar
    arquivo = open('ranking.txt', 'a')
    nome = input("\033[1;31;40m Digite seu nome: ")
    arquivo.write(f"{nome} {placar}\n")
    arquivo.close()

# def cria_barra_de_vida

def escolhe_monstro_de_um_vetor(enemies):
    finalistas = []
    for monstros_da_vez in range(0, len(enemies)):
        elegiveis = []
        for monstro in enemies[monstros_da_vez]:
            if monstro:
                elegiveis.append(monstro)
        if elegiveis:
            finalistas.append(choice(elegiveis))
    return choice(finalistas)

def reseta_inimigos(enemies):
    enemies = []
    global pontos
    global tamanho_colunas_maximo
    global tamanho_linhas_maximo
    global linhas_matriz
    global colunas_matriz
    pontos = 3

    if colunas_matriz <= tamanho_colunas_maximo:
        colunas_matriz+=1
    elif linhas_matriz<= tamanho_linhas_maximo:
        linhas_matriz+=1

 
    
def draw(ship, ship_shots, enemies, enemies_shots, window):
    global ship_hit
    if ship_hit:
        pisca_nave(ship, window)
    else:
        ship.draw()
        
    for shot in ship_shots:
        if shot:
            shot.draw()

    for i in range(0,len(enemies)):
        for j in enemies[i]:
            if j:
                j.update()
                j.draw()

    for shot in enemies_shots:
        if shot:
            shot.draw()

    
def pisca_nave(ship, window):
    global cronometro_piscada
    global desenha_nave
    
    cronometro_piscada += window.delta_time()
    if cronometro_piscada >= 0.2:
        desenha_nave = not desenha_nave
        cronometro_piscada = 0
    if desenha_nave:
        ship.draw()
            
def enemies_behavior(enemies, shots, window):
    global tiros_inimigo
    
    points_handler(window)
    if sem_inimigos(enemies):
        cria_inimigos(enemies)

    algum_inimigo_atira(window, enemies, tiros_inimigo)
    shots_behavior(tiros_inimigo, window, 1)
    colision_handle(window, enemies, shots)
    elimina_tiros_fora_da_tela(window, shots)
    
    move_inimigos(window, enemies)



def algum_inimigo_atira(window, enemies, tiros_inimigo):
    global cronometro_tiros_inimigo
    global distancia_entre_tiros_inimigo
    cronometro_tiros_inimigo += window.delta_time()
    if cronometro_tiros_inimigo >= distancia_entre_tiros_inimigo:
        cronometro_tiros_inimigo = 0
        inimigo_atira(window, escolhe_monstro_de_um_vetor(enemies), tiros_inimigo)

def inimigo_atira(window, enemie, tiros):
    novo_tiro = Sprite("tiro_inimigo.png")
    novo_tiro.set_position((enemie.x + enemie.width/2 - novo_tiro.width), (enemie.y + enemie.height))
    tiros.append(novo_tiro)


def points_handler(window):
    global pontos, cronometro_pontuacao
    if pontos>1:
        cronometro_pontuacao += window.delta_time()
        if cronometro_pontuacao >= 4:
            cronometro_pontuacao = 0
            pontos-=1

def sem_inimigos(enemies):
    for i in range(0, len(enemies)):
        for j in enemies[i]:
            if j:
                return False
    return True


def colision_handle(window, enemies, bullets):
    global placar
    global pontos
    for i in range(0, len(enemies)):
        for j in enemies[i]:
            for bullet in bullets:
                if j and bullet:
                    tiro_acertou_invader = Collision.collided(j, bullet)
                    if tiro_acertou_invader:
                        indice_invader = enemies[i].index(j)
                        enemies[i][indice_invader] = None
                        bullets[bullets.index(bullet)] = None
                        placar += pontos


def move_inimigos(window, enemies):
    global direcao_horizontal_inimigos
    global velocidade_inimigos
    
    

    if bateu_na_parede(enemies, window):
        direcao_horizontal_inimigos *= -1
        desce_inimigos(enemies, window)

    for i in range(0, len(enemies)):
        for j in enemies[i]:
            if j:
                j.move_x(velocidade_inimigos * window.delta_time() * direcao_horizontal_inimigos)

def bateu_na_parede(enemies, window):

    for i in range(0, len(enemies)):
        for j in enemies[i]:
            if j:
                bateu_na_parede_esquerda = (j.x <= 0)
                bateu_na_parede_direita = ((j.x + j.width) >= (window.width))
                bateu_na_parede = (bateu_na_parede_direita or bateu_na_parede_esquerda)
                if bateu_na_parede:
                    return True
    return False

def desce_inimigos(enemies, window):
    for i in range(0, len(enemies)):
        for j in enemies[i]:
            if j:
                j.y += 10   
                reposiciona_desfalcados(enemies, window)

def reposiciona_desfalcados(enemies, window):

    for i in range(0, len(enemies)):
        for j in enemies[i]:
            if j:
                atravessou_a_parede_esquerda = (j.x < 0)
                atravessou_a_parede_direita = ((j.x + j.width) > (window.width))

                if atravessou_a_parede_direita:
                    quanto_que_passou_da_parede_direita = (j.x + j.width) - window.width
                    j.x -= quanto_que_passou_da_parede_direita

                if atravessou_a_parede_esquerda:
                    quanto_que_passou_da_parede_esquerda = j.x * -1
                    j.x += quanto_que_passou_da_parede_esquerda


def cria_inimigos(vetor_de_inimigos):
    global linhas_matriz, colunas_matriz
    reseta_inimigos(vetor_de_inimigos)
    posicao_a_ser_inserido_no_eixo_y = 10
    for i in range(0,linhas_matriz):
        linha = []
        posicao_a_ser_inserido_no_eixo_x = 120
        for j in range(0,colunas_matriz):
            enemie = Sprite("invader.png", 3)
            enemie.set_total_duration(2000)
            enemie.set_position(posicao_a_ser_inserido_no_eixo_x, posicao_a_ser_inserido_no_eixo_y)
            linha.append(enemie)
            posicao_a_ser_inserido_no_eixo_x += enemie.width + 2
        vetor_de_inimigos.append(linha)
        posicao_a_ser_inserido_no_eixo_y += 62
        


def ship_behavior(ship, teclado, window, shots):
    shots_behavior(shots, window, -1)
    life_handle(ship, window)
    velocidade_nave = 200
    intervalo_entre_tiros_em_segundos = 0.3
    global tempo_desde_ultimo_tiro 
    
    tempo_desde_ultimo_tiro += window.delta_time()
    

    if teclado.key_pressed("left"):
        ta_antes_da_parede_esquerda = ship.x >= 0
        if ta_antes_da_parede_esquerda:
            ship.move_x(velocidade_nave * window.delta_time() * -1)

    if teclado.key_pressed("right"):
        ta_antes_da_parede_direita = ship.x < (window.width - ship.width)
        if ta_antes_da_parede_direita:
            ship.move_x(velocidade_nave * window.delta_time() * 1)

    if teclado.key_pressed("space"):
        if tempo_desde_ultimo_tiro >= intervalo_entre_tiros_em_segundos:
            tiro = Sprite("tiro.png")
            tiro.set_position(ship.x + (ship.width/2) - (tiro.width/2), window.height - ship.height - tiro.height)
            shots.append(tiro)
            tempo_desde_ultimo_tiro = 0
    elimina_tiros_fora_da_tela(window, shots)

def life_handle(ship, window):
    global tiros_inimigo
    global vidas
    global ship_hit
    global tempo_do_golpe
    for tiro in tiros_inimigo:
        if tiro:
            if Collision.collided(tiro, ship):
                tiros_inimigo.remove(tiro)
                ship_hit = True
                if vidas > 0:
                    vidas -=1
    if ship_hit:
        tempo_do_golpe += window.delta_time()
    if tempo_do_golpe >= 2:
        tempo_do_golpe = 0
        ship_hit = False


def elimina_tiros_fora_da_tela(window, shots):
    for shot in shots:
        if shot:
            if (shot.y <= 0) or (shot.y > window.height) :
                shots.pop(shots.index(shot))

def shots_behavior(shots, window, direcao):
    velocidade_tiro = 300
    for shot in shots:
        if shot:
            shot.move_y(velocidade_tiro * window.delta_time() * direcao)

from PPlay.window import *
from PPlay.sprite import *
from PPlay.gameimage import *

class Game(Window):
  gamestate = 0
  dificuldade = 1
  cor_fundo = (24,24,24)
  
  def __init__(self, width, height, title):
    self.window = Window(width, height)
    self.window.set_title(title)

  def is_end(self):
    self.window.close()

  def update(self):
    self.window.set_background_color(self.cor_fundo)
    self.window.update()

class Botao():
  
  def __init__(self, x, y, sprite_botao, sprite_botao_hover, funcao):
    self.default = Sprite(sprite_botao)
    self.hover = Sprite(sprite_botao_hover)
    self.default.set_position(x,y)
    self.hover.set_position(x,y)

  def draw(self, mouse, handler = False):
    x = self.default.x
    y = self.default.y
    largura = self.default.width
    altura = self.default.height

    mouse_over_button = mouse.is_over_area((x,y),(x + largura, y + altura))
    mouse_clicked = mouse.is_button_pressed(1)
    
    if mouse_over_button:
      self.hover.draw()
      handler_present = bool(handler)
      if mouse_clicked and handler_present:
          handler()

    else:
      self.default.draw()


class Menu(Sprite, GameImage):

  def __init__(self, fundo, botoes, game):
    self.botoes = botoes
    self.fundo = GameImage(fundo)

  def draw(self):
    self.fundo.draw()
    for i in self.botoes:
      i.draw()
  
  
    
